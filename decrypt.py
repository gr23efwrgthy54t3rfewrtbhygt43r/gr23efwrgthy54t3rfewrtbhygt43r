from cryptography.fernet import Fernet
import sys

key = sys.argv[2].decode()
fernet = Fernet(key)

with open("code.py.enc", "rb") as f:
    with open("code.py", "wb") as f2:
        f2.write(fernet.decrypt(f.read()))